class HomePageView
{
    constructor() {
        this.html = `
			<div>
				<h1 id="page-title">Demo project for course CITB558</h1>
				<div>
					<p>This is a demo single page application. It demonstrates the power of Js.</p>
					<p>Technologies used:</p>
					<p>
						<ul>
							<li>CSS3</li>
							<li>HTML5</li>
							<li>JavaScript ES5/ES6</li>
							<li>MongoDB (cloud based service Kinvey by Progress)</li>
						</ul>
					</p>
					<p>
						Although this project is not production ready due to it's clumsy implementation. But it's good for learning purposes.
					</p>
					<p>
						In order to run this project, you need to host it on a local server (Apache2, Nginx..). It wont work if you just open the index.html file
					</p>
				</div>
			</div>`;
        return this;
    }
}
