class BookView
{
    constructor() {
        this.html = `
			<div>
				<h1 id="books-title">All books</h1>
                <div class="list-group" data-booksView='list'></div>
			</div>`;

        this.listItemHtml = `<a href="#" class="list-group-item list-group-item-action"></a>`;

        return this;
    }
}
