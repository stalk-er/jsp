class BookSingleView
{
    constructor() {
        this.html = `
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <h2>Title</h2>
                            <h3 class="book-title"></h3>

                            <h2>Description</h2>
                            <p class="book-description"></p>

                            <h2>Price</h2>
                            <div>
                                <span class="book-price"></span><span> BGN</span>
                            </div>
                            <hr>
                            <p>Published by, <span class="book-publisher"></span> on <span class="book-datePublished"></span></p>
                        </div>
                    </div>
                </div>
            `;
    }
}
