class CreateBookView
{
    constructor() {
        
        this.html = `
            <section id="create-book-view" class="create-book-view">
                <h2>Create Book</h2>
                <br>
                <form id="create-book-form">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input required type="text" class="form-control" name="title" id="title" aria-describedby="titleHelp" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control rounded-0" placeholder="Description" id="description" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="datePublished">Date published</label>
                        <input required type="date" class="form-control" name="datePublished" id="datePublished" placeholder="Date published" />
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input required type="number" step="0.01" class="form-control" name="price" id="price" placeholder="Price" />
                    </div>
                    <button type="submit" class="btn btn-primary">Create</button>
                </form>
            </section>
        `;
        return this;
    }
}
