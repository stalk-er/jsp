/*
* Book Controller
*/
class BookController
{

    static createBookSubmit(e) {
        e.preventDefault();
        
        Requester.post("appdata", "books", {
            'title': $(this).find('input[name="title"]').val(),
            'datePublished': $(this).find('input[name="datePublished"]').val(),
            'description': $(this).find('textarea[name="description"]').val(),
            'price': $(this).find('input[name="price"]').val(),
            'publisher': app.user.username,
        })
        .done(function () {
            app.router.goTo('books', false);
        })
        .fail(function (data) {
        })
    }

    static renderCreateBookView() 
    {
        let self = this;
        return function()
        {
            var createBookView = new CreateBookView();
            var view = $(createBookView.html);

            view.find('form').submit(self.createBookSubmit);

            $('body').hide().fadeIn('slow');
            $(app.viewContainerSelector).html(view);
        }
    }

    static renderView()
    {

        return function(params)
        {
            var bookView = new BookView();
            var view = $(bookView.html);
            var viewListItem = $(bookView.listItemHtml);

            Requester.get( 'appdata', 'books')
            .done(function (books)
            {
                view.find('*[data-booksView]').empty();
                for(var a in books)
                    view.find('*[data-booksView]').append( viewListItem.clone().text(books[a].title).attr('href', 'book?id=' + books[a]._id) );
            });

            $('body').hide().fadeIn('slow');
            $(app.viewContainerSelector).html(view);
        }
    };

    static renderSingleView()
    {
        return function(params)
        {
            var singleView = new BookSingleView();
            var view = $(singleView.html);
            var query = JSON.stringify({ "_id": params['id'] });

            Requester.get( 'appdata', `books?query=${query}`)
            .done(function (book)
            {
                book = book[0];

                view.find('.book-title').text(book.title);
                view.find('.book-price').text(book.price);
                view.find('.book-publisher').text(book.publisher);
                view.find('.book-description').text(book.description);
                view.find('.book-datePublished').text(book.datePublished);
            });

            $('body').hide().fadeIn('slow');
            $(app.viewContainerSelector).html(view);
        }
    }
}
